﻿using EscapeMines.Library.Common.Enum;
using EscapeMines.Library.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace EscapeMines.Library.Models
{
    public class HeadDirection : IDirection
    {
        private directionEnum _direction;
        public HeadDirection()
        {
        }
        public directionEnum Direction { get
            {
                return _direction;
            }
            set {
                _direction = value;
            } 
        }

     
    }
}
