﻿using EscapeMines.Library.Common.Enum;
using EscapeMines.Library.Interfaces;
using System;
using System.Collections.Generic;


namespace EscapeMines.Library.Models
{
    public class Board : IBoard
    {
        private readonly List<BoardMark> _boardMarks;
        private readonly Size _boardSize;
        private readonly ICoordinates _startPosition;
        

        private readonly IGameConfiguration _configuration;
      
        public Board(IGameConfiguration configuration)
        {
            _configuration = configuration;   // coming from DI 
            _boardSize = _configuration.BoardSize();
            _startPosition = _configuration.StartingPosition();
            _boardMarks =             CreateMarks(_configuration.ExitPoint(), _configuration.MinesPositions());
        }

        public List<BoardMark> BoardMarks()
        {
            return _boardMarks;
        }

        public Size BoardSize()
        {
            return _boardSize;
        }
        public ICoordinates StartPosition()
        {
            return _startPosition;
        }

        private List<BoardMark> CreateMarks(ICoordinates exitTile, List<ICoordinates> minesPositions)
        {
            List<BoardMark> retMarks = new List<BoardMark>();
            retMarks.Add(new BoardMark() { Position = new Coordinates(exitTile.X, exitTile.Y), MarkType = BoardMarkType.ExitTile });
            foreach (Coordinates mine in minesPositions)
            {
                retMarks.Add(new BoardMark() { Position = new Coordinates(mine.X, mine.Y), MarkType = BoardMarkType.Mine });
            }
            return retMarks;
        }

    }

}
