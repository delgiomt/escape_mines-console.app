﻿using EscapeMines.Library.Common.Enum;
namespace EscapeMines.Library.Models
{
    public class BoardMark
    {
        public Coordinates Position { get; set; }
        public BoardMarkType MarkType { get; set; }
        public MarkHitResult Hit()
        {
            return this.MarkType switch
            {
                BoardMarkType.Mine => MarkHitResult.BlowUp,
                BoardMarkType.ExitTile => MarkHitResult.Exit,
                _ => MarkHitResult.NothingHappens,
            };
        }
    }
}
