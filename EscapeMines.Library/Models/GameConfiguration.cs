﻿using EscapeMines.Library.Common.Enum;
using EscapeMines.Library.Helpers;
using EscapeMines.Library.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;

namespace EscapeMines.Library.Models
{
    public class GameConfiguration : IGameConfiguration
    {

        private Size _boardSize;
        private ICoordinates _exitPosition;
        private IPawn _turtle;
        private List<ICoordinates> _mines;
        private List<string> _moves;
        public bool IsValid { get; set; }

        private readonly ILogger _logger;

        public GameConfiguration(ILoggerFactory logger)
        {
            _logger = logger.CreateLogger<GameConfiguration>();
            // read the config values from textFile
            SetConfigurationFromFile();
        }
         
        private void SetConfigurationFromFile()
        {
            string[] linesConfig = ReadConfigFile();
            _mines = new List<ICoordinates>();
            if (linesConfig != null)
            {
                List<string> moves = new List<string>() { };
                for (int row = 4; row < linesConfig.Length; row++)
                {
                    moves.Add(linesConfig[row]);
                }
                SetConfiguration(linesConfig[0], linesConfig[1], linesConfig[2], linesConfig[3], moves);
                Validate();
                IsValid = true;
            }
            else
            {
                _logger.LogError("Config file not found!");
                throw new FileNotFoundException("Config file not found!");
            }
        }

        private string[] ReadConfigFile(string configFile = "EscapeMines.conf")
        {
            string[] lines=null;
            if (File.Exists(configFile)) { lines = File.ReadAllLines(configFile); }
            return lines;
        }

        private void SetConfiguration(string boardSize,string mines,string exitPoint,string turtlePosition, List<string> moves)
        {
            string[] boardSizeConverted;
            boardSizeConverted = boardSize.Split(' ');
            _boardSize = new Size( Int32.Parse(boardSizeConverted[1]), Int32.Parse(boardSizeConverted[0]));  // rows x columns 

            string[] exitPositionConverted;
            exitPositionConverted = exitPoint.Split(' ');
            _exitPosition = new Coordinates(Int32.Parse(exitPositionConverted[0]), Int32.Parse(exitPositionConverted[1]));

            string[] turtlePositionConverted;
            turtlePositionConverted = turtlePosition.Split(' ');
            
            _turtle = new Turtle( new Coordinates(Int32.Parse(turtlePositionConverted[0]), Int32.Parse(turtlePositionConverted[1])), (directionEnum)Enum.Parse(typeof(directionEnum), turtlePositionConverted[2]));

            string[] minesPositions = mines.Split(' ');
            string[] minePosition;
            foreach (string mine in minesPositions)
            {
                minePosition = mine.Split(',');
                _mines.Add(new Coordinates(Int32.Parse(minePosition[0]), Int32.Parse(minePosition[1])));
            }
            _moves = moves;
        }

        public IPawn Pawn() 
            { 
                return _turtle;
            }

        public Size BoardSize()
        {
                return _boardSize;
        }

        public ICoordinates ExitPoint() 
        {
                return _exitPosition;
        }

        public List<ICoordinates> MinesPositions ()
            {
                return _mines;
            }
        
        public List<string> Moves ()
            {
                return _moves;
            }

        public ICoordinates StartingPosition()
            {
                return _turtle.Position();
            }
        public directionEnum  StartingDirection()
        {
            return _turtle.HeadDirection();
        }

        private void Validate()
        {
            if (_boardSize.Rows > 0 && _boardSize.Columns > 0)
            {
                if (!BoardHelpers.IsValidPosition(_boardSize, _exitPosition)) throw new ArgumentOutOfRangeException("Exit Tile position is outside the board");
                if (!BoardHelpers.IsValidPosition(_boardSize, _turtle.Position())) throw new ArgumentOutOfRangeException("Turtle position is outside the board");
                foreach (ICoordinates mine in _mines)
                {
                    if (!BoardHelpers.IsValidPosition(_boardSize, mine)) throw new ArgumentOutOfRangeException($"Mine position {mine.X}-{mine.Y} is outside the board");
                }
            }
            else throw new ArgumentOutOfRangeException("Board size not valid");
        }

        public void SetPawnPosition(ICoordinates coordinates, directionEnum direction)
        {
            _turtle.SetPosition(coordinates);
            _turtle.SetDirection(direction);
        }
    }
}
