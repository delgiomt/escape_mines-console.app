﻿using EscapeMines.Library.Interfaces;


namespace EscapeMines.Library.Models
{
    public class Coordinates : ICoordinates
    {  
        // note that X is row and Y is a column - are not a cartesian coordinates

        public Coordinates(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X { get; set; }
        public int Y { get; set; }

    }
}
