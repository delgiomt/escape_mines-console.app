﻿
using EscapeMines.Library.Common.Enum;
using EscapeMines.Library.Interfaces;
using System;

namespace EscapeMines.Library.Models
{
    public class Turtle: IPawn
    {
        static readonly string[] directions= Enum.GetNames(typeof(directionEnum));
        private directionEnum _direction;
        private  ICoordinates _position;

        event EventHandler AfterMoveEvent;

        public Turtle(ICoordinates position, directionEnum direction)
        {
            _position = position;
            _direction = direction;
        }

        event EventHandler IPawn.AfterMove
        {
            add
            {
                AfterMoveEvent += value;
            }

            remove
            {
                AfterMoveEvent -= value;
            }
        }

       

        public ICoordinates Position()
        { 
            return _position;
        }

        public void TurnRight()
        {
            int position = Array.IndexOf(directions, _direction.ToString());
            if (position<0) { throw new Exception("Current Turtle head direction non found"); }
            Enum.TryParse(directions[(position < directions.Length-1 ? position+1 :0)].ToString(), out _direction); 
        }

        public void TurnLeft()
        {
            int position = Array.IndexOf(directions, _direction.ToString());
            if (position < 0) { throw new Exception("Current Turtle head direction non found"); }
            Enum.TryParse(directions[(position >0  ? position - 1 : directions.Length-1)], out _direction);
        }
        // eventually we can define more directions. for example TurnUp TurnDown in case of multidimensional Board 3D for 3D game


        public void Move()    // note that X is row and Y is a column - are not a cartesian coordinates
        {
            _position=TryMove();
            AfterMoveEvent?.Invoke(this, EventArgs.Empty);
        }

        public ICoordinates TryMove()
        {
            Coordinates nextPosition = new Coordinates(_position.X,_position.Y);
            switch (_direction)
            {
                case directionEnum.N:
                    nextPosition.X--;
                    break;
                case directionEnum.E:
                    nextPosition.Y++;
                    break;
                case directionEnum.S:
                    nextPosition.X++;
                    break;
                case directionEnum.W:
                    nextPosition.Y--;
                    break;
            }
            return nextPosition;
        }


        void IPawn.SetDirection(directionEnum direction)
        {
            _direction = direction;
        }

        public directionEnum HeadDirection() 
            {
                return _direction;
            }
       public void SetPosition(ICoordinates position)
        {
            _position = position;
        }


    }
}
