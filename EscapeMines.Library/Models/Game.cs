﻿using EscapeMines.Library.Common.Enum;
using EscapeMines.Library.Helpers;
using EscapeMines.Library.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EscapeMines.Library.Models
{
    public class Game : IGame
    {
        private readonly IBoard _board;
        private readonly IGameConfiguration _configuration;
        private readonly IPawn _pawn;
        private int _nMoves = 0;

        public GameStatus Status { get; set; } = GameStatus.StillInDanger;
        

        public int NumberOfMoves {
            get
            {
                return _nMoves;
            }
        } 

        event EventHandler GameStatusChangedEvent;

        public  Game(IBoard board, IGameConfiguration configuration)
        {
            _board = board;
            _configuration = configuration;
            _pawn = new Turtle(configuration.StartingPosition(), configuration.StartingDirection());
            _pawn.AfterMove += Pawn_AfterMove;  
        }

        public GameStatus Move(MoveType movetype,int steps=1)
        {
            while (steps>0)
            {
                if (Status == GameStatus.StillInDanger)
                {
                    // actual move of the Pawn
                    switch (movetype)
                    {
                        case MoveType.RightTurn:
                            _pawn.TurnRight();
                            break;
                        case MoveType.LeftTurn:
                            _pawn.TurnLeft();
                            break;
                        case MoveType.Move:
                            // change Pawn position within the board
                            if (IsNextMoveWithinBoard(_pawn))
                            {
                                _pawn.Move();
                                _nMoves++;
                            }
                            else
                            { 
                                Status = GameStatus.PawnTryMoveOutsideTheBoard;
                                GameStatusChangedEvent?.Invoke(this, EventArgs.Empty);
                            }
                            break;
                    }
                }
                steps--;
            }
            return Status;
        }

        public string Play(List<string> movesToPlay = null)
        {
            if (movesToPlay == null) { movesToPlay = _configuration.Moves(); }
        
            string[] arrayMoves;
            foreach( string moves in movesToPlay )
            {
                arrayMoves = moves.Split(" ");
                for (int n=0;n<arrayMoves.Length;n++)
                {
                    switch (arrayMoves[n].ToUpper())
                    {
                        case "M":
                            Move(MoveType.Move);
                            break;
                        case "R":
                            Move(MoveType.RightTurn);
                            break;
                        case "L":
                            Move(MoveType.LeftTurn);
                            break;
                        default:
                            break;
                    }
                    if (Status != GameStatus.StillInDanger) { break; }
                }
                if (Status != GameStatus.StillInDanger ) { break; }
            }
            return Status.ToString();
        }
       
        /// <summary>
        /// this event happens when and after the Pawn moves - need to check if it hit a mine or exit tile
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Pawn_AfterMove(Object sender, EventArgs e)
        {
            GameStatus newStatus;
            BoardMark boardMarkCrossed = _board.BoardMarks().FirstOrDefault(mark=> mark.Position.X == _pawn.Position().X && mark.Position.Y == _pawn.Position().Y);
            if (boardMarkCrossed != null)
            {
                // Map the hit status to game result
                newStatus = boardMarkCrossed.Hit() switch
                {
                    MarkHitResult.BlowUp => GameStatus.MineHit,
                    MarkHitResult.Exit => GameStatus.Success,
                    _ => GameStatus.StillInDanger,
                };
                if (newStatus!=Status) {
                    Status = newStatus;
                    GameStatusChangedEvent?.Invoke(this, EventArgs.Empty); 
                }
            }
        }
       
        private bool IsNextMoveWithinBoard(IPawn pawn)
        {
            ICoordinates newPosition =  pawn.TryMove();
            if (!BoardHelpers.IsValidPosition(_board.BoardSize(), newPosition))
            {
                return false;
            }
            return true;
        }

        event EventHandler IGame.GameStatusChanged
        {
            add
            {
                GameStatusChangedEvent += value;
            }

            remove
            {
                GameStatusChangedEvent -= value;
            }
        }
    }
}
