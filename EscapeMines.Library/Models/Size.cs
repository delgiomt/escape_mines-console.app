﻿using System;

namespace EscapeMines.Library.Models
{
    public class Size
    {
        private int _row;
        private int _columns;

        public Size( int rows, int columns)
        {
            Rows = rows;
            Columns = columns;
        }

        # region PROPERTIES
        public int Rows { 
            get
            {
                return _row; 
            }
            set
            {
                if (value <= 0) { throw new ArgumentOutOfRangeException("row size not valid"); }
                _row = value;
            } 
        }

        public int Columns {
            get
            {
                return _columns;
            }
            set
            {
                if (value <= 0) { throw new ArgumentOutOfRangeException("column size not valid"); }
                _columns = value;
            }
        }
        # endregion

    }
}

