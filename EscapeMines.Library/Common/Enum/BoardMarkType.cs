﻿namespace EscapeMines.Library.Common.Enum
{
    public enum BoardMarkType
    {
        Mine,
        ExitTile
    }
}