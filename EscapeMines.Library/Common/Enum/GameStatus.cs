﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EscapeMines.Library.Common.Enum
{
    public enum GameStatus
    {
        Success,
        MineHit,
        StillInDanger,
        PawnTryMoveOutsideTheBoard
    }
}
