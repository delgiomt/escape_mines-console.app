﻿namespace EscapeMines.Library.Common.Enum
{
    public enum MoveType
    {
        RightTurn,
        LeftTurn,
        Move
    }
}
