﻿namespace EscapeMines.Library.Common.Enum
{
    // define the direction clockwise 
    public enum directionEnum
    {
        N,
        E,
        S,
        W
    }
}
