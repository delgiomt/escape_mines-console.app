﻿using EscapeMines.Library.Interfaces;
using EscapeMines.Library.Models;
namespace EscapeMines.Library.Helpers
{
    public static class BoardHelpers
    {
        public static bool IsValidPosition(Size boardSize, ICoordinates position)
        {
            // note that X is row and Y is a column - are not a cartesian coordinates
            if (position.X<0 || 
                position.Y<0 ||
                position.X >= boardSize.Rows || 
                position.Y >= boardSize.Columns) return false;
            return true;
        }
    }

    public static class PawnExtensionMethod
    {
        public static bool IsWithinBoard(this IPawn pawn, IBoard board) 
        {
            // note that X is row and Y is a column - are not a cartesian coordinates
            if (pawn.Position().X < 0 || pawn.Position().Y < 0 || pawn.Position().X >= board.BoardSize().Rows || pawn.Position().Y >= board.BoardSize().Columns)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }

}
