﻿using EscapeMines.Library.Common.Enum;
using EscapeMines.Library.Models;
using System;

namespace EscapeMines.Library.Interfaces
{
    public interface IPawn
    {
        void TurnRight();
        void TurnLeft();
        void Move();
        directionEnum HeadDirection();
        ICoordinates Position();
        event EventHandler AfterMove;
        void SetDirection(directionEnum direction);
        void SetPosition(ICoordinates position);
        ICoordinates TryMove();
    }
}
