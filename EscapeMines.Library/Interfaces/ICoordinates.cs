﻿namespace EscapeMines.Library.Interfaces
{
    public interface ICoordinates
    {
        int X { get; set; }
        int Y { get; set; }
     
    }
}