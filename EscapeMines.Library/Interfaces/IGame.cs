﻿using EscapeMines.Library.Models;
using System;
using System.Collections.Generic;

using System.Text;
using EscapeMines.Library.Common.Enum;
namespace EscapeMines.Library.Interfaces
{
    public interface IGame
    {
        public GameStatus Move(MoveType movetype, int steps=1);
        event EventHandler GameStatusChanged;
        public  int NumberOfMoves { get; }
        public GameStatus Status { get; }
        string Play(List<string> movesToPlay=null);
    }
}
