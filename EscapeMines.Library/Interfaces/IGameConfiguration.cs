﻿using EscapeMines.Library.Common.Enum;
using EscapeMines.Library.Models;
using System.Collections.Generic;

namespace EscapeMines.Library.Interfaces
{
    public interface IGameConfiguration
    {
        Size BoardSize();
        ICoordinates ExitPoint();
        List<ICoordinates> MinesPositions();
        List<string> Moves();
        IPawn Pawn();
        ICoordinates StartingPosition();
        directionEnum StartingDirection();
        void SetPawnPosition(ICoordinates coordinates, directionEnum direction);   // for testing porpouse
    }
}
