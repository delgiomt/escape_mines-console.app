﻿using EscapeMines.Library.Common.Enum;

namespace EscapeMines.Library.Interfaces
{
    public interface IDirection
    {
        directionEnum Direction { get; }
    }
}
