﻿using EscapeMines.Library.Common.Enum;
using EscapeMines.Library.Models;
using System.Collections.Generic;

namespace EscapeMines.Library.Interfaces
{ 
    public interface IBoard
    {
        ICoordinates StartPosition();
        Size BoardSize();
        List<BoardMark> BoardMarks();
    }
}