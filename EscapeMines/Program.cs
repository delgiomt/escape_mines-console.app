﻿
using EscapeMines.Library.Common.Enum;
using EscapeMines.Library.Models;
using EscapeMines.Library.Interfaces;
using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace EscapeMines
{
    class Program
    {
        private static IServiceProvider _serviceProvider;
        static void Main(string[] args)
        {
            RegisterServices();

            var logger = _serviceProvider.GetService<ILoggerFactory>()
                .CreateLogger<Program>();

            logger.LogInformation("Starting game application");

            var gameService = _serviceProvider.GetService<IGame>();

            logger.LogInformation($"Game result :{ gameService.Play().ToString()}");
            logger.LogInformation($"total number of moves : {gameService.NumberOfMoves}");
            
            DisposeServices();

            Console.WriteLine("Press any key to close the app.");
            Console.ReadKey();
        }

        private static void RegisterServices()
        {
            var serviceColl = new ServiceCollection()
                                           .AddLogging(cfg => cfg.AddConsole()).Configure<LoggerFilterOptions>(cfg => cfg.MinLevel = LogLevel.Debug)
                                           .AddSingleton<IGameConfiguration, GameConfiguration>()
                                           .AddSingleton<IBoard, Board>()
                                           .AddSingleton<IGame, Game>();
            serviceColl.AddSingleton<ICoordinates>(x => new Coordinates(0, 0));
            serviceColl.AddSingleton<IPawn>(t => new Turtle(t.GetService<ICoordinates>(), directionEnum.N));


            serviceColl.BuildServiceProvider();
            _serviceProvider = serviceColl.BuildServiceProvider();
        }
        private static void DisposeServices()
        {
            if (_serviceProvider == null)
            {
                return;
            }
            if (_serviceProvider is IDisposable)
            {
                ((IDisposable)_serviceProvider).Dispose();
            }
        }

    }
}

