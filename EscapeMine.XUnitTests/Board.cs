﻿using EscapeMines.Library.Common.Enum;
using EscapeMines.Library.Helpers;
using EscapeMines.Library.Models;
using Microsoft.Extensions.Logging.Abstractions;
using System.Linq;
using Xunit;

namespace EscapeMine.XUnitTests
{
    public class BoardTest
    {
        private readonly NullLoggerFactory _logger;
        public BoardTest()
        {
            _logger = new NullLoggerFactory();
        }

        [Fact]
        public void BoardFromConfigFile_mineBoardMarks_Exist()
        {
            // ARRANGE
            GameConfiguration gameConfiguration = new GameConfiguration(_logger);
            Board board = new Board(gameConfiguration);

            // ACT & ASSERT
            Assert.True(board.BoardMarks().Any(x => x.MarkType == BoardMarkType.Mine), "Mines marks not created");
        }

        [Fact]
        public void BoardFromConfigFile_ExitMark_OnlyOneExist()
        {
            // ARRANGE
            GameConfiguration gameConfiguration = new GameConfiguration(_logger);
            Board board = new Board(gameConfiguration);

            // ACT & ASSERT
            Assert.True(board.BoardMarks().Count(x => x.MarkType == BoardMarkType.ExitTile)==1, "Exit mark not exist or more than one");
        }



    }
}
