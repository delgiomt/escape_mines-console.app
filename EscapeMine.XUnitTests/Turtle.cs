﻿using EscapeMines.Library.Common.Enum;
using EscapeMines.Library.Helpers;
using EscapeMines.Library.Models;
using Microsoft.Extensions.Logging.Abstractions;

using Xunit;

namespace EscapeMine.XUnitTests
{
    public class TurtleTest
    {
        private readonly NullLoggerFactory _logger;
        public TurtleTest()
        {
            _logger = new NullLoggerFactory();
        }

        [Theory]
        [InlineData(2,3, directionEnum.N, directionEnum.E)]
        [InlineData(2, 3, directionEnum.W, directionEnum.N)]
        public void SetTurtleHead_TurnRight_ExpectedCorrectDirection (int x, int y, directionEnum directionTo, directionEnum directionExpected)
        {
            Turtle turtle = new Turtle(new Coordinates(x, y),directionTo);
            turtle.TurnRight();
            Assert.True(turtle.HeadDirection() == directionExpected, "turtle head direction incorrect");
        }


        [Theory]
        [InlineData(2, 3, directionEnum.N, directionEnum.W)]
        [InlineData(2, 3, directionEnum.S, directionEnum.E)]
        public void SetTurtleHead_TurnLeft_ExpectedCorrectDirection(int x, int y, directionEnum directionTo, directionEnum directionExpected)
        {
            Turtle turtle = new Turtle(new Coordinates(x, y), directionTo);
            turtle.TurnLeft();
            Assert.True(turtle.HeadDirection() == directionExpected, "turtle head direction incorrect");
        }


        [Theory]   // using a board 5 columns by 4 rows
        [InlineData(0, 0, directionEnum.N)]
        [InlineData(4, 2, directionEnum.E)]
        [InlineData(4, 3, directionEnum.S)]
        public void SetTurtlePos_Move_expectedOutOfBoard(int x, int y, directionEnum turtleDirection)
        {
            //ARRANGE
            GameConfiguration gameConfiguration = new GameConfiguration(_logger);
            Board board = new Board(gameConfiguration);

            // set the turtle in a test Coordinates next to a border   
            Turtle turtle = new Turtle(new Coordinates(x, y), turtleDirection);

            //ACT
            turtle.Move();
            
                //ASSERT
            // check that throw an error
            Assert.False(turtle.IsWithinBoard(board),"Turtle should be out of the board" );
        }

        [Theory]  
        [InlineData(0, 0, directionEnum.E,0,1)]
        [InlineData(3, 2, directionEnum.W,3,1)]
        [InlineData(1, 1, directionEnum.S,2,1)]
        public void SetTurtlePos_testMove_getTourtlePositionWhereExpected(int x, int y, directionEnum turtleDirection, int posX, int posY)
        {
            //ARRANGE
            //prepare a test board
           
            // set the turtle in a test Coordinates next to a border   
            Turtle turtle = new Turtle(new Coordinates(x, y), turtleDirection);
          
            //ACT
            // move the turtle
            turtle.Move();
            
            //ASSERT
            Assert.True(posX ==turtle.Position().X && posY==turtle.Position().Y,"Turtle position not in the position where it was expected ");
        }


        [Theory]   
        [InlineData(3, 2)]
        [InlineData(1, 0)]
        [InlineData(3, 4)]
        [InlineData(3, 3)]
        [InlineData(0, 3)]
        public void SetTurtlePosition_testExtensionMethod_isWithinBoard(int x, int y)
        {
            // ARRANGE
            GameConfiguration gameConfiguration = new GameConfiguration(_logger);
            Board board = new Board(gameConfiguration);
            Turtle turtle = new Turtle(new Coordinates(0, 0), directionEnum.N);
            // ACT
            turtle.SetPosition(new Coordinates(x, y));

            // ASSERT
            Assert.True(turtle.IsWithinBoard(board), "Turtle expected within the board ()");

        }
        [Theory]  
        [InlineData(-1, 0)]
        [InlineData(5, 4)]
        [InlineData(11, 3)]
        [InlineData(3, 11)]
        public void SetTurtlePosition_testExtensionMethod_isOutsideBoard(int x, int y)
        {
            // ARRANGE
            GameConfiguration gameConfiguration = new GameConfiguration(_logger);
            Board board = new Board(gameConfiguration);
            Turtle turtle = new Turtle(new Coordinates(0, 0), directionEnum.N);
            // ACT
            turtle.SetPosition(new Coordinates(x, y));

            // ASSERT
            Assert.False(turtle.IsWithinBoard(board), "Turtle expected outside the board ()");

        }
    }
}
