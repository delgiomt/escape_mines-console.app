﻿using EscapeMines.Library.Models;
using EscapeMines.Library.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EscapeMine.XUnitTests
{
    public class BoardHelperTest
    {
        [Theory]
        [InlineData(2, 5,1,1)]
        [InlineData(5, 5, 4, 4)]
        [InlineData(34, 335, 32, 300)]
        [InlineData(5, 5, 0, 0)]
        public void BoardSize_checkValidPosition(int rows, int columns , int x, int y)
        {
            // ARRANGE
            Size boardSize = new Size( rows,columns );

            // ACT
            Coordinates position = new Coordinates(x, y);

            // ASSERT
            Assert.True(BoardHelpers.IsValidPosition(boardSize,position),  "Position outside the board");
        }

        [Theory]
        [InlineData(2, 5, 2, 5)]
        [InlineData(5, 5, 12, 23)]
        public void BoardSize_checkValidPosition_PositionOutsideBoard( int rows,int columns, int x, int y)
        {
            // ARRANGE
            Size boardSize = new Size(rows,columns);
            
            // ACT
            Coordinates position = new Coordinates(x, y);

            // ASSERT
            Assert.False(BoardHelpers.IsValidPosition(boardSize, position), "Position inside the board");
        }
    }
}
