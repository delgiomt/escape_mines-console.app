﻿using EscapeMines.Library.Models;
using Microsoft.Extensions.Logging.Abstractions;
using Xunit;

namespace EscapeMine.XUnitTests
{
    public class ConfigurationTest
    {
        private readonly NullLoggerFactory _logger;
        public ConfigurationTest()
        {
            _logger = new NullLoggerFactory();
        }
        [Fact]
        public void ReadCorrectConfigurationFile_notThroughtException()
        {
            // ARRANGE
            GameConfiguration gameConfiguration = new GameConfiguration(_logger);

            // ACT & ASSERT
            Assert.True(gameConfiguration.IsValid, "Configuration not valid");
        }
    }
}
