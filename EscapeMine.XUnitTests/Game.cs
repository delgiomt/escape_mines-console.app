﻿
using EscapeMines.Library.Common.Enum;
using EscapeMines.Library.Interfaces;
using EscapeMines.Library.Models;
using System;
using Xunit;
using Microsoft.Extensions.Logging.Abstractions;
using System.Collections.Generic;

namespace EscapeMine.XUnitTests
{

    public class GameTest
    {
        private readonly NullLoggerFactory _logger;
        public GameTest()
        {
            _logger = new NullLoggerFactory();
        }

        [Fact]
        public void StartUpGame_seriesOfMoves_TurtleHitMine()
        {
            // ARRANGE
            string statsChanged = "";
            GameStatus gameStatus = GameStatus.StillInDanger;
            GameConfiguration gameConfiguration = new GameConfiguration(_logger);

            Board board = new Board(gameConfiguration);
            IGame game = new Game(board, gameConfiguration);

            game.GameStatusChanged += (object sender, EventArgs e) => {
                statsChanged = "yes";
                gameStatus = game.Status;
            };


            // ACT
            game.Move(EscapeMines.Library.Common.Enum.MoveType.Move);
            game.Move(EscapeMines.Library.Common.Enum.MoveType.RightTurn);
            game.Move(EscapeMines.Library.Common.Enum.MoveType.Move);
            game.Move(EscapeMines.Library.Common.Enum.MoveType.Move);
            game.Move(EscapeMines.Library.Common.Enum.MoveType.RightTurn);
            game.Move(EscapeMines.Library.Common.Enum.MoveType.Move);
            game.Move(EscapeMines.Library.Common.Enum.MoveType.RightTurn);
            game.Move(EscapeMines.Library.Common.Enum.MoveType.Move);
            game.Move(EscapeMines.Library.Common.Enum.MoveType.Move, 3);

            // ASSERT
            Assert.True(gameStatus == GameStatus.MineHit, "Turtle doesn't blow up as expected");
            Assert.True(statsChanged == "yes", "Event statusChanged not fired");

            // Extras
            Assert.True(game.NumberOfMoves == 5, $"Number of moves should be 5 (instead of {game.NumberOfMoves}) even if ask to move 7 because the Turtle hit a mine");
        }

        [Theory]
        [InlineData("R M")]
        [InlineData("R R M M L M M M")]
        [InlineData("R R M M L M M L M R M L M")]
        public void StartUpGame_Play_TurtleHitMine(string moves)
        {
            string statsChanged = "";
            GameStatus gameStatus = GameStatus.StillInDanger;
            GameConfiguration gameConfiguration = new GameConfiguration(_logger);

            Board board = new Board(gameConfiguration);
            IGame game = new Game(board, gameConfiguration);

            game.GameStatusChanged += (object sender, EventArgs e) => {
                statsChanged = "yes";
                gameStatus = game.Status;
            };


            // ACT
            game.Play(new List<String>() {moves});
            
            // ASSERT
            Assert.True(gameStatus == GameStatus.MineHit, "Turtle doesn't blow up as expected");
            Assert.True(statsChanged == "yes", "Event statusChanged not fired");
        }

        [Theory]
        [InlineData("M R M L M M M")]
        [InlineData("R R R M M M M")]
        public void PlayGame_MoveOutsideBoard_getCorrectStatus(string moves)
        {
            string statsChanged = "";
            GameStatus gameStatus = GameStatus.StillInDanger;
            GameConfiguration gameConfiguration = new GameConfiguration(_logger);

            Board board = new Board(gameConfiguration);
            IGame game = new Game(board, gameConfiguration);

            game.GameStatusChanged += (object sender, EventArgs e) => {
                statsChanged = "yes";
                gameStatus = game.Status;
            };

            // ACT
            game.Play(new List<String>() { moves });

            // ASSERT
            Assert.True(gameStatus == GameStatus.PawnTryMoveOutsideTheBoard, "Turtle not outside the board as expected");
            Assert.True(statsChanged == "yes", "Event statusChanged not fired");
        }


        [Theory]
        [InlineData(0,0,directionEnum.N)]
        [InlineData(0, 0, directionEnum.W)]
        [InlineData(0, 4, directionEnum.N)]  // assuming board 5x4
        [InlineData(0, 4, directionEnum.E)]  // assuming board 5x4
        [InlineData(3, 4, directionEnum.S)]  // assuming board 5x4
        [InlineData(3, 4, directionEnum.E)]  // assuming board 5x4
        public void MovePawnOutsideBoard_getRightStatus(int x, int y, directionEnum direction)
        {
            //ARRANGE
            GameConfiguration gameConfiguration = new GameConfiguration(_logger);
            gameConfiguration.SetPawnPosition(new Coordinates(x,y), direction);
            Board board = new Board(gameConfiguration);
            IGame game = new Game(board, gameConfiguration);

            // ACT
            game.Move(MoveType.Move);

            // ASSERT
            Assert.True(game.Status==GameStatus.PawnTryMoveOutsideTheBoard, "Turtle expected outside the board");

        }

        [Theory]
        [InlineData(0, 0, directionEnum.S)]
        [InlineData(0, 0, directionEnum.E)]
        [InlineData(0, 4, directionEnum.S)]  // assuming board 5x4
        [InlineData(0, 4, directionEnum.W)]  // assuming board 5x4
        [InlineData(3, 4, directionEnum.N)]  // assuming board 5x4
        [InlineData(3, 4, directionEnum.W)]  // assuming board 5x4
        public void MovePawnInsideBoard_getRightStatus(int x, int y, directionEnum direction)
        {
            //ARRANGE
            GameConfiguration gameConfiguration = new GameConfiguration(_logger);
            gameConfiguration.SetPawnPosition(new Coordinates(x, y), direction);
            Board board = new Board(gameConfiguration);
            IGame game = new Game(board, gameConfiguration);

            // ACT
            game.Move(MoveType.Move);

            // ASSERT
            Assert.False(game.Status == GameStatus.PawnTryMoveOutsideTheBoard, "Turtle expected outside the board");

        }
    }
}

