using EscapeMines.Library.Models;
using System;
using Xunit;

namespace EscapeMine.XUnitTests
{
    public class SizeAndCoordinatesTest
    {
        [Theory]
        [InlineData(0, 5)]
        [InlineData(5, 0)]
        [InlineData(-30, -5)]
        public void Set_WrongBoardSize_ArgumentOutOfRangeException(int rows,int columns )
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => new Size(rows,columns));
        }

        [Theory]
        [InlineData(2, 5)]
        [InlineData(1, 1)]
        [InlineData(30, 400)]
        public void Set_CorrectBoardSize_CheckRowsMatch( int rows,int columns)
        {
            Size size = new Size(rows, columns);
            Assert.True(size.Rows == rows, "Rows not match");
        }

        [Theory]
        [InlineData(2, 5)]
        [InlineData(1, 1)]
        [InlineData(30, 400)]
        public void Set_CorrectBoardSize_CheckColumnsMatch(int rows, int columns )
        {
            Size size = new Size( rows,columns);
            Assert.True(size.Columns == columns, "Columns not match");
        }

        [Theory]
        [InlineData(2, 5)]
        [InlineData(1, 1)]
        [InlineData(30, 400)]
        public void Set_CorrectCoordinates_CheckXMatch(int x, int y)
        {
            Coordinates position = new Coordinates(x, y);
            Assert.True(position.X == x, "x value not match");
        }

        [Theory]
        [InlineData(2, 5)]
        [InlineData(1, 1)]
        [InlineData(30, 400)]
        public void Set_CorrectCoordinates_CheckYMatch(int x, int y)
        {
            Coordinates position = new Coordinates(x, y);
            Assert.True(position.Y == y, "y value not match");
        }
    }
}
